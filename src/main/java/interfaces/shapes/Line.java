package interfaces.shapes;

public class Line {
  private double length;

  public Line(double length) {
    this.length = length;
  }

  public double getLength() {
    return length;
  }

  public void setLength(double length) {
    this.length = length;
  }
}
