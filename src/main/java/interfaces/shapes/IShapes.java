package interfaces.shapes;

public interface IShapes {

  /**
   * Method for getting a description of the shape
   * @return A String that contains a basic description of the shape
   */
  public String returnDescription();

  /**
   * Method for getting the number of sides on a 2D shape
   * @return The number of sides of the 2D shape
   */
  public int returnNumberOfShapeSides();

  /**
   * Method for getting the circumference of a 2D shape
   * @return The circumference of the 2D shape
   */
  public double calculateCircumference();

  /**
   * Method for getting the area of a 2D shape
   * @return The area of the 2D shape
   */
  public double calculateArea();

  /**
   * Method for comparing the area of two 2D shapes
   * @return True if the area of the two shapes are equal, False if it is not
   */
  public boolean hasSameArea(IShapes shape);

  /**
   * Method for comparing the circumference of two 2D shapes
   * @return True if the circumference of the two shapes are equal, False if it is not
   */
  public boolean hasSameCircumference();

}
